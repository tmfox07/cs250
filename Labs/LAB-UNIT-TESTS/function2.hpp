#ifndef _function2
#define _function2

int SumArray( int arr[], int size )
{
	int sum = 0;
    for ( int i = 0; i < size; i++ )
    {
       
        sum += arr[i];
    }
    return sum;
}

/* Add a test to this function */
void Test_SumArr()
{
    cout << "************ Test_SumArr ************" << endl;

    int input1, input2, input3;
    int expectedOutput;
    int actualOutput;

    /* TEST 1 ********************************************/
    int inputArray1[] = { 1, 2, 3, 4 };
    expectedOutput = 10;

    actualOutput = SumArray( inputArray1, 4 );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_SumArr: Test 1 passed!" << endl;
    }
    else
    {
        cout << "Test_SumArr: Test 1 FAILED! \n\t"
        << "Inputs: 1, 2, 3, 4 \n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 2 ********************************************/
    // CREATE YOUR OWN TEST
    input1 = 7;             // change me
    input2 = 6;             // change me
    input3 = 07;             // change me
    expectedOutput = 20;    // change me


    // Run test (keep this as-is):
    actualOutput = SumThree( input1, input2, input3 );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_SumArr: Test 2 passed!" << endl;
    }
    else
    {
        cout << "Test_SumArr: Test 2 FAILED! \n\t"
        << "Inputs: PUT YOUR INPUTS HERE \n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 3 ********************************************/
    // CREATE YOUR OWN TEST
    input1 = 25;             // change me
    input2 = 27;             // change me
    input3 = 23;             // change me
    expectedOutput = 75;    // change me


    // Run test (keep this as-is):
    actualOutput = SumThree( input1, input2, input3 );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_SumArr: Test 3 passed!" << endl;
    }
    else
    {
        cout << "Test_SumArr: Test 3 FAILED! \n\t"
        << "Inputs: PUT YOUR INPUTS HERE \n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }
}

#endif

/**
example output
***************************************
**              TESTER               **
***************************************
1. Test SumThree
2. Test SumArr
3. Test IsOverdrawn
4. Test GetLength
5. Quit

Test which function? 2


************ Test_SumArr ************
Test_SumArr: Test 1 passed!
Test_SumArr: Test 2 passed!
Test_SumArr: Test 3 passed!


Press enter to continue...


**/

