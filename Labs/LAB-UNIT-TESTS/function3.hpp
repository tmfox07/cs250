#ifndef _function3
#define _function3

bool IsOverdrawn( float balance )
{
    if ( balance < 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Test_IsOverdrawn()
{
    cout << "************ Test_IsOverdrawn ************" << endl;

    float input;
    bool expectedOutput;
    bool actualOutput;

    /* TEST 1 ********************************************/
    input = 0;
    expectedOutput = false;

    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_IsOverdrawn: Test 1 passed!" << endl << endl;
    }
    else
    {
        cout << "Test_IsOverdrawn: Test 1 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 2 ********************************************/
    // CREATE YOUR OWN TEST
    input = -5;                  // change me
    expectedOutput = true;     // change me


    // Run test (keep this as-is):
    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_AddThree: Test 2 passed!" << endl;
    }
    else
    {
        cout << "Test_AddThree: Test 2 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 3 ********************************************/
    // CREATE YOUR OWN TEST
    input = 30;                  // change me
    expectedOutput = false;     // change me


    // Run test (keep this as-is):
    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_AddThree: Test 3 passed!" << endl;
    }
    else
    {
        cout << "Test_AddThree: Test 3 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }
}

#endif

/**
example output
***************************************
**              TESTER               **
***************************************
1. Test SumThree
2. Test SumArr
3. Test IsOverdrawn
4. Test GetLength
5. Quit

Test which function? 3


************ Test_IsOverdrawn ************
Test_IsOverdrawn: Test 1 passed!

Test_AddThree: Test 2 passed!
Test_AddThree: Test 3 passed!


Press enter to continue...


**/

