#ifndef _function4
#define _function4

#include <string>
using namespace std;

int GetLength(string word)
{
    int length = word.size();
    return length;
}

void Test_GetLength()
{
    cout << "************ Test_GetLength ************" << endl;

    string input;
    int expectedOutput;
    int actualOutput;

    /* TEST 1 ********************************************/
    input = "cat";
    expectedOutput = 3;

    actualOutput = GetLength( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_GetLength: Test 1 passed!" << endl << endl;
    }
    else
    {
        cout << "Test_GetLength: Test 1 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 2 ********************************************/
    // CREATE YOUR OWN TEST
    input = "bear";             // change me
    expectedOutput = 4;     // change me

    actualOutput = GetLength( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_GetLength: Test 1 passed!" << endl;
    }
    else
    {
        cout << "Test_GetLength: Test 1 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 3 ********************************************/
    // CREATE YOUR OWN TEST
    input = "robin";             // change me
    expectedOutput = 5;     // change me

    actualOutput = GetLength( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_GetLength: Test 1 passed!" << endl;
    }
    else
    {
        cout << "Test_GetLength: Test 1 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }
}

#endif

/**
Example output
***************************************
**              TESTER               **
***************************************
1. Test SumThree
2. Test SumArr
3. Test IsOverdrawn
4. Test GetLength
5. Quit

Test which function? 4


************ Test_GetLength ************
Test_GetLength: Test 1 passed!

Test_GetLength: Test 1 passed!
Test_GetLength: Test 1 passed!


Press enter to continue...

**/



