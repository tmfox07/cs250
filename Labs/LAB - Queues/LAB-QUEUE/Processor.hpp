#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;

	int cycles = 0;
	int totalTime = 0;
	int averageTime = (totalTime / jobQueue.Size());

	while (jobQueue.Size() > 0)
	{
		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, FCFS);//set the front -most item's finish time
			jobQueue.Pop();// pop the item off the jobQueue
		}

		cycles++;
	}
	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "Job ID: " << allJobs[i].id
			<< "\t"
			<< "Time to Complete: " << allJobs[i].fcfs_finishTime
			<< endl;
		
	}
	output << " Total time: ........ " << cycles << endl;
	output << "Average time: ....... " << (cycles / allJobs.size());

	output.close();

}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin " << endl;
	int timer = 0;
	int cycles = 0;

	while (jobQueue.Size() > 0)
	{
		if (timer = timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted += 1;
			jobQueue.Push( jobQueue.Front() );
			jobQueue.Pop();
			timer = 0;
		}
		jobQueue.Front()->Work(RR);
		if (jobQueue.Front()->rr_done )
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);
			jobQueue.Pop();
		}
		cycles++;
		timer++;
	}
	

	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "Job ID: " << allJobs[i].id << "\t"
			<< "Time to Complete: " << allJobs[i].rr_finishTime << "\t"
			<< "Times Interrupted: " << allJobs[i].rr_timesInterrupted << "\t"
			<< endl;
		
	}
	output << "Total Time: ............ " << cycles << endl;
	output << "Average Time: .......... " << cycles / allJobs.size() << endl;
	output << "Round Robin intervals: .." << timer << endl;
	output.close();
	
}

#endif
