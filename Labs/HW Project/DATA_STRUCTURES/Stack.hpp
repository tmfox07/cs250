#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
public:
	LinkedStack()
	{
		m_itemCount = 0;
		m_array = nullptr;
	}

	~LinkedStack()
	{
		if (m_array != nullptr)
		{
			delete[] m_array;
		}
	}

	void Push(const T& newData)
	{
		if (m_array == nullptr)
		{
			Allocate(100);
		}
		else if (m_itemCount == m_arraySize)
		{
			Resize();
		}

		m_array[m_itemCount] = newData;
		m_itemCount++;
	}

	T& Top()
	{
		if (m_array == nullptr || m_itemCount == 0)
		{
			throw runtime_error("Array is empty");
		}

		return m_array[m_itemCount - 1];
	}

	void Pop()
	{
		if (m_itemCount > 0)
		{
			m_itemCount--;
		}
	}

	int Size()
	{
		return m_itemCount;
	}

private:
	void Resize()
	{
		T * tempArray = new T[m_arraySize + 10];

		for (int i = 0; i < m_itemCount; i++)
		{
			tempArray[i] = m_array[i];
		}

		delete[] m_array;

		m_array = tempArray;

		m_arraySize = m_arraySize + 10;
	}

	void Allocate(int size)
	{
		m_array = new T[size];
	}
	T * m_array;
	Node<T>* m_ptrFirst;
	Node<T>* m_ptrLast;
	int m_itemCount;
	int m_arraySize;
};

#endif
