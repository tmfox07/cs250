#include "Tester.hpp"
#include <iostream>
#include <string>
using namespace std;

void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();	
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;
	List<string> testlist;
	testlist.Size();
	if (testlist.Size() == 0)
	{
		cout << "Test Passes" << endl;
	}
	else
	{
		cout << "Test Fails" << endl;
	}
	
	// Put tests here
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	// Put tests here
	{
		cout << endl << "Test 1:  Create a List and add 99 items;  shift right should return true..." << endl;
		List<string> testList;
		for (int i = 0; i < 98; i++)
		{
			testList.PushFront("Z");
		}
		bool expectedValue = true;
		bool actualValue = testList.ShiftRight(0);

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a List and add 100 items;  shift right should return false..." << endl;
		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushFront("Z");
		}
		bool expectedValue = false;
		bool actualValue = testList.ShiftRight(0);

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;

	// Put tests here
	{
		cout << endl << "Test 1:  Create a List and add 100 items;  use input 0 and shift left should return true..." << endl;
		List<string> testList;
		for (int i = 0; i < 99; i++)
		{
			testList.PushBack("Z");
		}
		bool expectedValue = true;
		bool actualValue = testList.ShiftLeft(0);

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a List and add 100 items; use input 101 and shift left should return false" << endl;
		List<string> testList;
		for (int i = 0; i < 99; i++)
		{
			testList.PushFront("Z");
		}
		bool expectedValue = false;
		bool actualValue = testList.ShiftLeft(101);

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;
	cout << "Prerequisite: Implement PushBack" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	// Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;
		List<int> ListA;
		bool expectedValue = true;
		bool actualValue = ListA.IsEmpty();

		cout << "Created List, didn't add anything, should be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 1" << endl;
		List<int> ListA;
		ListA.PushBack(5);
		bool expectedValue = false;
		bool actualValue = ListA.IsEmpty();

		cout << "Created List, added one thing, shouldn't be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;

	// Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;
		List<int> ListA;
		bool expectedValue = false;
		bool actualValue = ListA.IsFull();

		cout << "Created List, didn't add anything, should not be full..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 1" << endl;
		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack("Z");
		}
		bool expectedValue = true;
		bool actualValue = testList.IsFull();

		cout << "Created List, added 100 items,  should be full..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}


void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	// Put tests here
	{
		cout << endl << "Test 1, create a List and insert one item, PushFront() should return true" << endl;

		List<string> testList;
		bool expectedValue = true;
		bool actualValue = testList.PushFront("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 1b, create a List and insert one item, Size() should return 1" << endl;

		List<string> testList;
		testList.PushFront("A");

		int expectedValue = 1;
		int actualValue = testList.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2, create a List, insert 101 items, PushFront() should return false" << endl;

		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushFront("Z");
		}

		bool expectedValue = false;
		bool actualValue = testList.PushFront("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2b, create a List, insert 101 items, Size() should return 100" << endl;

		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushFront("Z");
		}
		testList.PushFront("A");

		int expectedValue = 100;
		int actualValue = testList.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 3, insert 'A', 'B', 'C', make sure 0 = C, 1 = B, 2 = A" << endl;

		List<string> testList;
		testList.PushFront("A");
		testList.PushFront("B");
		testList.PushFront("C");

		string expectedValue_0 = "C";
		string expectedValue_1 = "B";
		string expectedValue_2 = "A";

		string* actualValue_0 = testList.Get(0);
		string* actualValue_1 = testList.Get(1);
		string* actualValue_2 = testList.Get(2);

		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "no segfaults plz" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "Failed" << endl;
		}
		else
		{
			cout << "Pass" << endl;
		}
	}
}


void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	// Put tests here
	{
		cout << endl << "Test 1, create a List and insert one item, PushBack() should return true" << endl;

		List<string> testList;
		bool expectedValue = true;
		bool actualValue = testList.PushBack("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 1b, create a List and insert one item, Size() should return 1" << endl;

		List<string> testList;
		testList.PushBack("A");

		int expectedValue = 1;
		int actualValue = testList.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2, create a List, insert 101 items, PushBack() should return false" << endl;

		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack("Z");
		}

		bool expectedValue = false;
		bool actualValue = testList.PushBack("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2b, create a List, insert 101 items, Size() should return 100" << endl;

		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack("Z");
		}
		testList.PushBack("A");

		int expectedValue = 100;
		int actualValue = testList.Size();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 3, insert 'A', 'B', 'C', make sure 0 = A, 1 = B, 2 = C" << endl;

		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		string expectedValue_0 = "A";
		string expectedValue_1 = "B";
		string expectedValue_2 = "C";

		string* actualValue_0 = testList.Get(0);
		string* actualValue_1 = testList.Get(1);
		string* actualValue_2 = testList.Get(2);

		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "no segfaults plz" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "Failed" << endl;
		}
		else
		{
			cout << "Pass" << endl;
		}
	}
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;

	// Put tests here
	{
		cout << endl << "Test 1:  Create an empty List,  PopFront() should return false..." << endl;
		List<string> testList;
		bool expectedValue = false;
		bool actualValue = testList.PopFront();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2: Creat a List and insert A, B, C, D;  PopFront() should return true..." << endl
			<< "Prerequisite: PushBack()" << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("D");

		bool expectedValue = true;
		bool actualValue = testList.PopFront();

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2b: Create a List and insert A, B, C, D; Use GetFront() to return B..." << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("D");
		testList.PopFront();

		string expectedValue = "B";
		string *actualValue = testList.GetFront();

		if (actualValue == nullptr)
		{
			cout << "segfault advoidance" << endl;
			return;
		}
		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << *actualValue << endl;

		if (expectedValue == *actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;

	// Put tests here
	{
		cout << endl << "Test 1:  Create an empty List,  PopBack() should return false..." << endl;
		List<string> testList;

		bool expectedValue = false;
		bool actualValue = testList.PopBack();

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
	{
		cout << endl << "Test 2: Creat a List and insert A, B, C, D;  PopFront() should return true..." << endl
			<< "Prerequisite: PushBack()" << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("D");

		bool expectedValue = true;
		bool actualValue = testList.PopBack();

		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		cout << endl << "Test 2b: Create a List and insert A, B, C, D; Use GetBack() to return C..." << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("D");
		testList.PopBack();

		string expectedValue = "C";
		string *actualValue = testList.GetBack();

		if (actualValue == nullptr)
		{
			cout << "segfault advoidance" << endl;
			return;
		}
		cout << "Expected Value: " << expectedValue << endl;
		cout << "Actual Value: " << *actualValue << endl;

		if (expectedValue == *actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_clear" << endl;

	// Put tests here
	{
		cout << endl << "Test 1:  Create an empty List... clear() should return true..." << endl;
		List<string> testList;
		testList.Clear();
		int expectedValue = 0;
		int actualValue = testList.Size();

		cout << "The expected value: " << expectedValue << endl
			<< "The actual value: " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test Passes" << endl;
		}
		else
		{
			cout << "Test Fails" << endl;
		}

		{
			cout << endl << "Test 2: Create a partial List... Size() should return 0..." << endl;
			List<string> testList;
			for (int i = 0; i < 50; i++)
			{
				testList.PushFront("X");
			}
			testList.Clear();

			int expectedValue = 0;
			int actualValue = testList.Size();

			cout << "The expected value: " << expectedValue << endl
				<< "The actual value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 3: Create a full List... Size() should return 0..." << endl;
			List<string> testList;
			for (int i = 0; i < 99; i++)
			{
				testList.PushFront("X");
			}
			testList.Clear();
			int expectedValue = 0;
			int actualValue = testList.Size();

			cout << "The expected value: " << expectedValue << endl
				<< "The actual value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

	}
}

	void Tester::Test_Get()
	{
		DrawLine();
		cout << "TEST: Test_Get" << endl;

		// Put tests here
		{
			cout << endl << "Test 1: Create an empty List... Get(1) returns nullptr" << endl;

			List<string> testList;
			string *expectedValue = nullptr;
			string *actualValue = testList.Get(1);

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 1: Create an List and PushBack 1, 2, 3, 4... Get(1) returns 2" << endl;

			List<int> testList;
			testList.PushBack(1);
			testList.PushBack(2);
			testList.PushBack(3);
			testList.PushBack(4);

			int expectedValue = 2;
			int* actualValue = testList.Get(1);

			if (actualValue == nullptr)
			{
				cout << "segfault advoidance" << endl;
				return;
			}
			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << *actualValue << endl;

			if (expectedValue == *actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}
	}

	void Tester::Test_GetFront()
	{
		DrawLine();
		cout << "TEST: Test_GetFront" << endl;

		// Put tests here
		{
			cout << endl << "Test 1: Create an empty List... GetFront() returns nullptr" << endl;

			List<string> testList;
			string *expectedValue = nullptr;
			string *actualValue = testList.GetFront();

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2: Create an List and PushBack A, B, C, D... GetFront() returns A " << endl;

			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			testList.PushBack("D");

			string expectedValue = "A";
			string *actualValue = testList.GetFront();
			if (actualValue == nullptr)
			{
				cout << "segfault advoidance" << endl;
				return;
			}

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << *actualValue << endl;

			if (expectedValue == *actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

	}

	void Tester::Test_GetBack()
	{
		DrawLine();
		cout << "TEST: Test_GetBack" << endl;

		// Put tests here
		{
			cout << endl << "Test 1: Create an empty List... GetBack() returns nullptr" << endl;

			List<string> testList;
			string *expectedValue = nullptr;
			string *actualValue = testList.GetBack();

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2: Create an List and PushBack A, B, C, D... GetBack() returns D " << endl;

			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			testList.PushBack("D");

			string expectedValue = "D";
			string *actualValue = testList.GetBack();
			if (actualValue == nullptr)
			{
				cout << "segfault advoidance" << endl;
				return;
			}

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << *actualValue << endl;

			if (expectedValue == *actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

	}

	void Tester::Test_GetCountOf()
	{
		DrawLine();
		cout << "TEST: Test_GetCountOf" << endl;

		// Put tests here
		{
			cout << endl << "Test 1: Create an empty List... GetCountOf(A) returns 0" << endl;
			List<string> testList;

			int expectedValue = 0;
			int actualValue = testList.GetCountOf("A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2: Create a List and insert A,B,C... GetCountOf(A) returns 1" << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");

			int expectedValue = 1;
			int actualValue = testList.GetCountOf("A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 23: Create a List and insert A, A, B, C... GetCountOf(A) returns 2" << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");

			int expectedValue = 2;
			int actualValue = testList.GetCountOf("A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}
	}

	void Tester::Test_Contains()
	{
		DrawLine();
		cout << "TEST: Test_Contains" << endl;

		// Put tests here
		{
			cout << endl << "Test 1:  Create an empty List... Contains(A) returns false..." << endl;
			List<string> testList;
			bool expectedValue = false;
			
			bool actualValue = testList.Contains("A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2:  Create a List and insert A, B, C ... Contains(A) returns true..." << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			bool expectedValue = true;
			bool actualValue = testList.Contains("A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 1:  Create a List and insert A, B, C ... Contains(D) returns false..." << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			bool expectedValue = false;
			bool actualValue = testList.Contains("D");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}



	}

	void Tester::Test_Remove()
	{
		DrawLine();
		cout << "TEST: Test_Remove" << endl;

		// Put tests here
		{
			cout << endl << "Test 1:  Create an empty List... Remove(1) returns false..." << endl;
			List<string> testList;
			bool expectedValue = false;
			bool actualValue = testList.RemoveIndex(1);

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2:  Create a List and insert A,B,C,D... Remove(1) returns true..." << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			testList.PushBack("D");
			bool expectedValue = true;
			bool actualValue = testList.RemoveIndex(1);

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2:  Create a List and insert A,B,C,D... Remove(0) then GetFront returns B" << endl;
			List<string> testList;
			testList.PushBack("A");
			testList.PushBack("B");
			testList.PushBack("C");
			testList.PushBack("D");
			testList.RemoveIndex(0);
			string expectedValue = "B";
			string *actualValue = testList.GetFront();

			if (actualValue == nullptr)
			{
				cout << "segfault advoidance" << endl;
				return;
			}
			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << *actualValue << endl;

			if (expectedValue == *actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}
	}

	void Tester::Test_Insert()
	{
		DrawLine();
		cout << "TEST: Test_Insert" << endl;

		// Put tests here
		{
			cout << endl << "Test 1:  Create a List and add 5 items.  Insert A at index 3... Get 3 returns A..." << endl;
			List<string> testList;
			for (int i = 0; i < 5; i++)
			{
				testList.PushBack("X");
			}
			testList.Insert(3, "A");

			string expectedValue = "A";
			string *actualValue = testList.Get(3);

			if (actualValue == nullptr)
			{
				cout << "segfault advoidance" << endl;
				return;
			}
			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << *actualValue << endl;

			if (expectedValue == *actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

		{
			cout << endl << "Test 2:  Create a List and add 100 items.  Insert A at index 101... Insert() returns false..." << endl;
			List<string> testList;
			for (int i = 0; i < 99; i++)
			{
				testList.PushBack("X");
			}

			bool expectedValue = false;
			bool actualValue = testList.Insert(101, "A");

			cout << "Expected Value: " << expectedValue << endl
				<< "Actual Value: " << actualValue << endl;

			if (expectedValue == actualValue)
			{
				cout << "Test Passes" << endl;
			}
			else
			{
				cout << "Test Fails" << endl;
			}
		}

	}