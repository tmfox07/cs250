// Lab - Standard Template Library - Part 3 - Queues
// Tim Fox

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{

	queue<float> account;
	float balance = 0;
	bool done = false;
	while (!done)
	{
		cout << "Enter transaction amount or enter 9999 to continue " << endl;
		float amt;
		cin >> amt;
		
		if (amt != 9999)
		{
			account.push(amt);
			
			balance += amt;
		}
		else
		{
			done = true;
		}
	}
	cout << "The total balance is : "
		 << balance << endl;


	cin.ignore();
	cin.get();
	return 0;
}

/**
Sample Output
Enter transaction amount or enter 9999 to continue
56.75
Enter transaction amount or enter 9999 to continue
98.25
Enter transaction amount or enter 9999 to continue
36.25
Enter transaction amount or enter 9999 to continue
-45.76
Enter transaction amount or enter 9999 to continue
9999
The total balance is : 145.49
**/