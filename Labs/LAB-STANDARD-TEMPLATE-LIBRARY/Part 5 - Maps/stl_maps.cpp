// Lab - Standard Template Library - Part 5 - Maps
// Tim Fox

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;

	colors['r'] = "FFOOOO";
	colors['g'] = "OOFFOO";
	colors['b'] = "OOOOFF";
	colors['c'] = "OOFFFF";
	colors['m'] = "FFOOFF";
	colors['y'] = "FFFFOO";

	bool done = false;
	while (!done)
	{
		char color;
		cout << "Enter a color letter or 'q' to stop:  " << endl;
		cin >> color;
		if (color != 'q')
		{
			cout << "Hex: " << colors[color] << endl;
			cout << '\n';
			
		}
		else
		{
			done = true;
		}
	}
	cout << "Goodbye" << endl;

    cin.ignore();
    cin.get();
    return 0;
}

/**
example output
Enter a color letter or 'q' to stop:
r
Hex: FFOOOO

Enter a color letter or 'q' to stop:
g
Hex: OOFFOO

Enter a color letter or 'q' to stop:
b
Hex: OOOOFF

Enter a color letter or 'q' to stop:
c
Hex: OOFFFF

Enter a color letter or 'q' to stop:
m
Hex: FFOOFF

Enter a color letter or 'q' to stop:
y
Hex: FFFFOO

Enter a color letter or 'q' to stop:
t
Hex:

Enter a color letter or 'q' to stop:
q
Goodbye
**/
