// Lab - Standard Template Library - Part 4 - Stacks
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> word;
	bool done = false;
	while (!done)
	{
		cout << "Enter the next letter of the word, or UNDO to undo, or DONE to stop" << endl;
		string choice;
		cin >> choice;

		if (choice == "UNDO" || choice == "undo")
		{
			word.pop();
		}
		else if (choice == "DONE" || choice == "done")     // stops the program
		{
			done = true;
		}
		else
		{
			word.push(choice);
		}
		}
	while (!word.empty())
	{
		cout << " " << word.top();
		word.pop();
	}
	cout << '\n';



    cin.ignore();
    cin.get();
    return 0;
}

/** 
Sample Output
Enter the next letter of the word, or UNDO to undo, or DONE to stop
e
Enter the next letter of the word, or UNDO to undo, or DONE to stop
r
Enter the next letter of the word, or UNDO to undo, or DONE to stop
r
Enter the next letter of the word, or UNDO to undo, or DONE to stop
undo
Enter the next letter of the word, or UNDO to undo, or DONE to stop
e
Enter the next letter of the word, or UNDO to undo, or DONE to stop
h
Enter the next letter of the word, or UNDO to undo, or DONE to stop
t
Enter the next letter of the word, or UNDO to undo, or DONE to stop
-
Enter the next letter of the word, or UNDO to undo, or DONE to stop
h
Enter the next letter of the word, or UNDO to undo, or DONE to stop
undo
Enter the next letter of the word, or UNDO to undo, or DONE to stop
o
Enter the next letter of the word, or UNDO to undo, or DONE to stop
l
Enter the next letter of the word, or UNDO to undo, or DONE to stop
l
Enter the next letter of the word, or UNDO to undo, or DONE to stop
e
Enter the next letter of the word, or UNDO to undo, or DONE to stop
h
Enter the next letter of the word, or UNDO to undo, or DONE to stop
done
h e l l o - t h e r e
**/
