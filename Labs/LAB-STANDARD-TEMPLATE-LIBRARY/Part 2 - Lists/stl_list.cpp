// Lab - Standard Template Library - Part 2 - Lists
// Tim Fox

#include <iostream>
#include <list>
#include <string>
using namespace std;

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "1. Push front "
			<< "2. Push back "
			<< "3. Pop front "
			<< "4. Pop back"
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:				// push front
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
		case 2:				// push back
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}
		case 3:				//pop front
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.pop_front(state);
			break;
		}
		case 4:				// pop back
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.pop_back(state);
			break;
		}
		
		case 5:
		{

			bool done = true;
			break;
		}
	}
	
	cin.ignore();
    cin.get();
    return 0;
}
