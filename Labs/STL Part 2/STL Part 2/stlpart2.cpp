// Lab - Standard Template Library - Part 2 - Lists
// Tim Fox

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states)
{
	for (
		/* intitialization code*/
		list<string>::iterator it = states.begin();
		/* keep looping while */
		it != states.end();
		/*run after each iteration*/
		it++
		)
	{
		// Display this element of the list
		cout << *it << "\t";
	}
	cout << endl;
}

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "1. Push front "
			<< "2. Push back "
			<< "3. Pop front "
			<< "4. Pop back "
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:				// push front
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
		case 2:				// push back
		{
			cout << "enter a new state: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}
		case 3:				//pop front
		{
			
			states.pop_front();
			break;
		}
		case 4:				// pop back
		{
		
			states.pop_back();
			break;
		}

		case 5:
		{

			done = true;
			
		}
		}
	}

		cout << "Original : " << endl;
		DisplayList(states);

		cout << "Reversed: " << endl;
		states.reverse();
		DisplayList(states);

		cout << "Sorted" << endl;
		states.sort();
		DisplayList(states);

		cout << "sorted-reverse: " << endl;
		states.reverse();
		DisplayList(states);

		cin.ignore();
		cin.get();
		return 0;
	}